import glob
import os
import pathlib
import subprocess
from subprocess import PIPE, Popen
import pycparser_fake_libc

import cffi

wrk_dir = pathlib.Path(__file__).parent.resolve() / "wrk"

patterns = ("*.o", "*.so", "*.obj", "*.dll", "*.exp", "*.lib", "*.c")
for file_pattern in patterns:
    for file in glob.glob(file_pattern):
        os.remove(file)

subprocess.call(["make"], cwd=wrk_dir)

ffi = cffi.FFI()

headers = ["main.h"]

ffi_header = ""
for h in headers:
    location = wrk_dir / "src" / h
    ffi_header += f'#include "{location.as_posix()}"' + "\n"

command = [
    "gcc",
    "-P",
    "-E",
    "-I",
    "obj/include/luajit-2.1",
    "-I",
    "obj/include/openssl",
    "-I",
    pycparser_fake_libc.directory,
    "-",
]
p = Popen(command, stdin=PIPE, stdout=PIPE, cwd=wrk_dir)
definitions = p.communicate(input=ffi_header.encode())[0].decode()

definitions = definitions.split(";")
parsed_definitions = []
for d in definitions:
    if d.strip().startswith("extern"):
        continue
    if d.strip().startswith("static"):
        continue
    parsed_definitions.append(d)
definitions = ";".join(parsed_definitions)

ffi.cdef(
    """
int main(int, char **);
typedef struct ssl_ctx_st SSL_CTX;
typedef struct ssl_st SSL;
"""
)

ffi.cdef(definitions)


ffi.set_source(
    "_wrk_cffi",
    ffi_header,
    libraries=["ssl", "crypto", "luajit-5.1", "m", "pthread", "dl"],
    library_dirs=[(wrk_dir / "obj" / "lib").as_posix()],
    extra_objects=[
        "wrk/obj/wrk.o",
        "wrk/obj/ae.o",
        "wrk/obj/aprintf.o",
        "wrk/obj/bytecode.o",
        "wrk/obj/http_parser.o",
        "wrk/obj/net.o",
        "wrk/obj/script.o",
        "wrk/obj/ssl.o",
        "wrk/obj/stats.o",
        "wrk/obj/units.o",
        "wrk/obj/version.o",
        "wrk/obj/zmalloc.o",
    ],
    include_dirs=[
        (wrk_dir / "obj" / "include" / "luajit-2.1").as_posix(),
        (wrk_dir / "obj" / "include" / "openssl").as_posix(),
    ],
)

ffi.compile(verbose=True)

import _wrk_cffi

print(dir(_wrk_cffi.lib))
